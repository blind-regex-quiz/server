// Copyright (C) 2021 Ivan Efimov, https://gitlab.com/ivan_efimov

package main

import (
	"encoding/json"
	"flag"
	"github.com/google/uuid"
	"log"
	"net/http"
	"strconv"
)

const ( // Коды завершения для API
	CodeOK              = 0
	CodeInternalError   = 1
	CodeTooManySessions = 2
	CodeNoSuchSession   = 3
	CodeBadParams       = 4
	CodeNoAttemptsLeft  = 5
)

/*
	Описание API:
	Все методы возвращают код `http.StatusOK`, если вернулся другой код, то это проблема соединения
	или внутренняя ошибка сервера

	1.
	/api/next_question
	Создаёт новый вопрос
	Не имеет параметров
	Возвращает json-объект:
	{
		code: int
		id: string
		variants: [ string ]
		probes_left: int
	}
	> code -- код завершения запроса, если 0 (CodeOK), то новый вопрос сгенерирован успешно и поля id, variants,
		probes_left содержат валидные значения
		допустимые значения: CodeOK, CodeInternalError, CodeTooManySessions
	> id -- уникальный идентификатор сессии (каждая сессия соответствует одному вопросу и имеет ограниченное время жизни)
	> variants -- массив из строк, содержащий варианты ответа
	> probes_left -- количество оставшихся использований пробного текста

	2.
	/api/validate_text
	Проверяет, подходит ли пробный текст для вопроса с указанным id сессии
	Параметры:
		id: string -- обязательный, id сессии вопроса
		text: string -- обязательный, пробный текст
	Возвращает json-объект:
	{
		code: int
		result: boolean
		probes_left: int
	}
	> code -- код завершения запроса, если 0 (CodeOK), то запрос выполнен успешно и поля result, probes_left содержат
		валидные значения
		допустимые значения: CodeOk, CodeInternalError, CodeNoSuchSession, CodeBadParams, CodeNoAttemptsLeft
	> result -- результат сопоставления "правильного ответа" c пробным текстом
	> probes_left -- количество оставшихся использований пробного текста

	3.
	/api/validate_answer
	Проверяет, является ли переданный вариант ответа (по индексу, начиная с 0) "правильным ответом" для
	вопроса с указанным id сессии, приводит к удалению сессии вопроса
	Параметры:
		id: string -- обязательный, id сессии вопроса
		variant: int -- обязательный, индекс варианта ответа (начиная с 0)
	Возвращает json-объект:
	{
		code: int
		result: boolean
		correct_variant: int
	}
	> code -- код завершения запроса, если 0 (CodeOK), то запрос выполнен успешно и поля result, correct_variant содержат
		валидные значения
		допустимые значения: CodeOk, CodeInternalError, CodeNoSuchSession, CodeBadParams
	> result -- результат проверки "правильности" переданного варианта ответа,
	> correct_variant -- индекс правильного варианта ответа
*/

var sessionManager *SessionManager

func main() {
	addr := flag.String("addr", "0.0.0.0", "listen address")
	port := flag.Uint("port", 33535, "listen port")

	flag.Parse()

	sessionManager = NewSessionManager()

	http.HandleFunc("/api/next_question", handleNextQuestion)
	http.HandleFunc("/api/validate_text", handleValidateText)
	http.HandleFunc("/api/validate_answer", handleValidateAnswer)
	log.Fatalln(http.ListenAndServe(*addr+":"+strconv.FormatUint(uint64(*port), 10), nil))
}

type newSessionResponse struct {
	Code       int      `json:"code"`
	Id         string   `json:"id"`
	Variants   []string `json:"variants"`
	ProbesLeft int      `json:"probes_left"`
}

type validateTextResponse struct {
	Code       int  `json:"code"`
	Result     bool `json:"result"`
	ProbesLeft int  `json:"probes_left"`
}

type validateAnswerResponse struct {
	Code          int  `json:"code"`
	Result        bool `json:"result"`
	CorrectAnswer int  `json:"correct_answer"`
}

func handleNextQuestion(w http.ResponseWriter, r *http.Request) {
	resp := newSessionResponse{}

	sId, err := sessionManager.NewSession()
	if err == nil {
		variants, probesCount, err := sessionManager.GetVariants(sId)
		if err == nil {
			resp.Code = CodeOK
			resp.Id = sId.String()
			resp.Variants = variants
			resp.ProbesLeft = probesCount
		} else {
			resp.Code = CodeInternalError
		}
	} else {
		switch err.(type) {
		case *TooManySessionsError:
			resp.Code = CodeTooManySessions
		default:
			resp.Code = CodeInternalError
		}
	}

	w.WriteHeader(http.StatusOK)
	b, _ := json.Marshal(resp)
	_, _ = w.Write(b)
}

func handleValidateText(w http.ResponseWriter, r *http.Request) {
	resp := validateTextResponse{}

	sIdRaw := r.URL.Query().Get("id")
	text := r.URL.Query().Get("text")
	if sId, err := uuid.Parse(sIdRaw); err == nil {
		result, probesLeft, err := sessionManager.Probe(sId, text)
		if err == nil {
			resp.Code = CodeOK
			resp.Result = result
			resp.ProbesLeft = probesLeft
		} else {
			switch err.(type) {
			case NoSuchSessionError:
				resp.Code = CodeNoSuchSession
			case NoAttemptsLeftError:
				resp.Code = CodeNoAttemptsLeft
			default:
				resp.Code = CodeInternalError
			}
		}
	} else {
		resp.Code = CodeBadParams
	}

	w.WriteHeader(http.StatusOK)
	b, _ := json.Marshal(resp)
	_, _ = w.Write(b)
}

func handleValidateAnswer(w http.ResponseWriter, r *http.Request) {
	resp := validateAnswerResponse{}

	sIdRaw := r.URL.Query().Get("id")
	variantRaw := r.URL.Query().Get("variant")
	if sId, err := uuid.Parse(sIdRaw); err == nil {
		if variant, err := strconv.ParseInt(variantRaw, 10, 32); err == nil {
			result, correct, err := sessionManager.CheckAnswer(sId, int(variant))
			if err == nil {
				resp.Code = CodeOK
				resp.Result = result
				resp.CorrectAnswer = correct
				sessionManager.DeleteSession(sId)
			} else {
				switch err.(type) {
				case NoSuchSessionError:
					resp.Code = CodeNoSuchSession
				default:
					resp.Code = CodeInternalError
				}
			}
		} else {
			resp.Code = CodeBadParams
		}
	} else {
		resp.Code = CodeBadParams
	}

	w.WriteHeader(http.StatusOK)
	b, _ := json.Marshal(resp)
	_, _ = w.Write(b)
}
