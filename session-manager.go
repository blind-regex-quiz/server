// Copyright (C) 2021 Ivan Efimov, https://gitlab.com/ivan_efimov

package main

import (
	"github.com/google/uuid"
	"math/rand"
	"regexp"
	"sync"
	"time"
)

const DefaultMaxSessions = 128
const DefaultSessionLifetime = 5 * time.Minute
const DefaultVariantsCount = 4
const DefaultProbesCount = 2

const RENodesCount = 3
const REMaxQuantity = 5
const REQuantificationProbability = 0.3

type TooManySessionsError struct{}

func (er TooManySessionsError) Error() string { return "too many sessions" }

type NoSuchSessionError struct{}

func (er NoSuchSessionError) Error() string { return "no such session" }

type NoAttemptsLeftError struct{}

func (er NoAttemptsLeftError) Error() string { return "no attempts left" }

type Session struct {
	id             uuid.UUID
	variants       []string
	probesCount    int
	correctVariant int
	re             *regexp.Regexp
	timer          *time.Timer
	sm             *SessionManager
}

func (s *Session) BeginTimer() {
	go func() {
		<-s.timer.C
		s.sm.DeleteSession(s.id)
	}()
}

type SessionManager struct {
	mutex           sync.RWMutex
	sessions        map[uuid.UUID]*Session
	maxSessions     int
	sessionLifetime time.Duration
	variantsCount   int
}

func NewSessionManager() *SessionManager {
	sm := &SessionManager{
		mutex:           sync.RWMutex{},
		sessions:        make(map[uuid.UUID]*Session),
		maxSessions:     DefaultMaxSessions,
		sessionLifetime: DefaultSessionLifetime,
		variantsCount:   DefaultVariantsCount,
	}
	return sm
}

func (sm *SessionManager) NewSession() (uuid.UUID, error) {
	if len(sm.sessions) >= sm.maxSessions {
		return uuid.UUID{}, TooManySessionsError{}
	}
	id := uuid.New()
	newSession := &Session{
		id:             id,
		variants:       []string{},
		probesCount:    DefaultProbesCount,
		re:             nil,
		correctVariant: rand.Intn(sm.variantsCount),
		timer:          time.NewTimer(sm.sessionLifetime),
		sm:             sm,
	}
	for i := 0; i < sm.variantsCount; i++ {
		newSession.variants = append(newSession.variants, NewRegex(
			RENodesCount,
			REMaxQuantity,
			REQuantificationProbability,
		))
	}
	re, err := regexp.Compile(newSession.variants[newSession.correctVariant])
	if err != nil {
		return uuid.UUID{}, err
	}
	newSession.re = re
	newSession.BeginTimer()
	sm.mutex.Lock()
	sm.sessions[id] = newSession
	sm.mutex.Unlock()

	return id, nil
}

func (sm *SessionManager) DeleteSession(id uuid.UUID) {
	sm.mutex.Lock()
	delete(sm.sessions, id)
	sm.mutex.Unlock()
}

func (sm *SessionManager) Probe(id uuid.UUID, str string) (matchResult bool, probesLeft int, err error) {
	sm.mutex.RLock()
	defer sm.mutex.RUnlock()
	session, ok := sm.sessions[id]
	if !ok {
		return false, 0, NoSuchSessionError{}
	}
	if session.probesCount > 0 {
		session.probesCount--
		return session.re.MatchString(str), session.probesCount, nil
	} else {
		return false, 0, NoAttemptsLeftError{}
	}
}

func (sm *SessionManager) CheckAnswer(id uuid.UUID, variant int) (checkResult bool, correctVariant int, err error) {
	sm.mutex.RLock()
	defer sm.mutex.RUnlock()
	session, ok := sm.sessions[id]
	if !ok {
		return false, 0, NoSuchSessionError{}
	}
	return session.correctVariant == variant, session.correctVariant, nil
}

func (sm *SessionManager) GetVariants(id uuid.UUID) (variants []string, probesCount int, err error) {
	sm.mutex.RLock()
	defer sm.mutex.RUnlock()
	session, ok := sm.sessions[id]
	if !ok {
		return nil, 0, NoSuchSessionError{}
	}
	return session.variants, session.probesCount, nil
}
