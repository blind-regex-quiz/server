// Copyright (C) 2021 Ivan Efimov, https://gitlab.com/ivan_efimov

package main

import (
	"fmt"
	"math/rand"
	"strings"
)

const charsetAlphaNum = "abcdefghijklmnopqrstuvwxyz" + "ABCDEFGHIJKLMNOPQRSTUVWXYZ" + "1234567890"

var charsetSpecial = []string{
	"\\w",
	"\\W",
	"\\d",
	"\\D",
	"\\s",
	"\\S",
}

type Expression struct {
	StrBefore string
	StrAfter  string
}

type ExpressionNode struct {
	value    Expression
	children []*ExpressionNode
}

func (node *ExpressionNode) GetString() string {
	str := node.value.StrBefore
	for _, child := range node.children {
		str += child.GetString()
	}
	str += node.value.StrAfter
	return str
}

// Генерирует регулярное выражение по заданным параметрам.
// Сперва генерируется length элементарных узлов (символы и классы символов)
// Потом они группируются произвольным образом с единичным уровнем вложенности
// Каждой группе в вероятностью quantificationProbability добавляется квантификатор
//
func NewRegex(length int, maxQuantity int, quantificationProbability float32) string {
	if length == 0 {
		return ""
	}
	if quantificationProbability < 0 {
		quantificationProbability = 0
	}
	if quantificationProbability > 1 {
		quantificationProbability = 1
	}

	var nodes []string
	for i := 0; i < length; i++ {
		nodes = append(nodes, newSimpleExpression())
	}
	var nodesGrouped []string
	for i := 0; i < len(nodes); i++ {
		groupSize := rand.Intn(len(nodes) - i + 1)
		group := ""
		if groupSize <= 1 {
			group = nodes[i]
		} else {
			for j := 0; j < groupSize; j++ {
				group += nodes[i+j]
			}
			i += groupSize - 1
			group = "(" + group + ")"
		}
		if rand.Float32() < quantificationProbability {
			group += newQuantifier(maxQuantity)
		}
		nodesGrouped = append(nodesGrouped, group)
	}

	return strings.Join(nodesGrouped, "")
}

func newSimpleExpression() string {
	if rand.Intn(4) == 0 {
		// Диапазон
		// Генерируем два числа a, b, 0 <= a < b < 26 (размер английского алфавита)
		width := 1 + rand.Intn(25)
		a := rand.Intn(26 - width) // Такое число, чтобы a + width < 26
		b := a + width
		return fmt.Sprintf("[%c-%c]", []rune(charsetAlphaNum)[a], []rune(charsetAlphaNum)[b])
	} else {
		// Один символ
		if rand.Intn(2) == 0 {
			return charsetSpecial[rand.Intn(len(charsetSpecial))]
		} else {
			return fmt.Sprintf("%c", []rune(charsetAlphaNum)[rand.Intn(len(charsetAlphaNum))])
		}
	}
}

func newQuantifier(maxQuantity int) string {
	quantificationType := rand.Intn(4)
	switch quantificationType {
	case 0:
		return "*"
	case 1:
		return "+"
	case 2:
		return "?"
	default:
	}
	str := "{"
	width := 1 + rand.Intn(maxQuantity-1)
	a := rand.Intn(maxQuantity - width) // Такое число, чтобы a + width < 26
	b := a + width
	if a != 0 {
		str += fmt.Sprintf("%d", a)
	}
	str += fmt.Sprintf(",%d}", b)
	return str
}
