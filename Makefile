SOURCES=main.go session-manager.go regex-generator.go

get_deps:
	go get github.com/google/uuid

build:
	go build -o regex-quiz-server $(SOURCES)

fmt:
	gofmt -w $(SOURCES)

all: get_deps build